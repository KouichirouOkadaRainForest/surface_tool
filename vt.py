# pip install virustotal-python

import os
import sys
import glob
import zlib
import json
import time
import pefile

from virustotal_python import Virustotal
from pprint import pprint

vtotal = Virustotal(API_KEY="e545d4cdfde2e046f15bcb3fbc43e1b7987127bbb700e87e551de7cd4afc90be", API_VERSION="v3")

for filename in glob.glob("/home/ubuntu/test_data/*") :
    print( filename )
    sys.stdout.flush()

    result = {}
    str_object1 = open(filename, 'rb').read()
    str_object2 = zlib.decompress(str_object1)
    f = open('tmp', 'wb')
    f.write(str_object2)
    f.close()

    FILE_ID = os.path.basename( filename )
    try :
        resp = vtotal.request(f"files/{FILE_ID}")
        result = resp.data
    except :
        result = "request error"
    
    pprint(result)

    f = open(os.path.join("vt_result",FILE_ID+".json"),"w")
    json.dump( result, f, indent=4 )
    f.close()

    time.sleep(30.0)

