# pip install virustotal-python

import os
import sys
import glob
import zlib
import json
import time
import pefile
import subprocess

from pprint import pprint

#--------------------------------------
def res_cmd(cmd):
  return subprocess.Popen(
      cmd, stdout=subprocess.PIPE,
      shell=True).communicate()[0].decode('utf-8')
#--------------------------------------

for filename in glob.glob("/home/ubuntu/test_data/*") :
    print( filename )
    sys.stdout.flush()

    result = {}
    str_object1 = open(filename, 'rb').read()
    str_object2 = zlib.decompress(str_object1)
    f = open('tmp', 'wb')
    f.write(str_object2)
    f.close()

    FILE_ID = os.path.basename( filename )
    str_data = res_cmd("strings tmp")
    pprint(FILE_ID)

    f = open(os.path.join("str_result",FILE_ID+".txt"),"w")
    f.write( str_data )
    f.close()

