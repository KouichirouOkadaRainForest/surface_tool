#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main( int argc,char *argv[] ) {
	struct stat st;
	char *fileName = argv[1];

	stat(fileName, &st);
	char *buffer = (char *)malloc( st.st_size );

	FILE *fp = fopen( fileName, "rb");
	fread(buffer, 1, st.st_size, fp);
	fclose(fp);

	// 0x84       0x0   Machine:                       0x0  
	// 0xDC       0x44  Subsystem:                     0x0 
	short *target_1;
	short *target_2;
	target_1 = (short*)(buffer + 0x84);
	printf("Machine: %x\n", *target_1);

	target_2 = (short*)(buffer + 0xDC);
	printf("Subsystem: %x\n", *target_2);

	*target_1 = 0x14c;
	*target_2 = 2;
	fp = fopen( "tmp_1", "wb");
	fwrite(buffer, 1, st.st_size, fp);
	fclose(fp);

//	*target_1 = 0x8664;
//	*target_2 = 2;
//	fp = fopen( "tmp_2", "wb");
//	fwrite(buffer, 1, st.st_size, fp);
//	fclose(fp);

	free( buffer );
}
