#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
import zlib
import json
import pefile

filename = sys.argv[1]
sys.stdout.flush()

result = {}
str_object1 = open(filename, 'rb').read()
str_object2 = zlib.decompress(str_object1)
f = open('tmp', 'wb')
f.write(str_object2)
f.close()

result["hash"] = os.path.basename( filename )
pe =  pefile.PE("tmp")
result["section"] = []
for section in pe.sections:
    try :
        result["section"].append( {
            "Name": section.Name.decode('utf-8'),
            "VirtualAddress": hex(section.VirtualAddress),
            "Misc_VirtualSize": hex(section.Misc_VirtualSize),
            "SizeOfRawData": section.SizeOfRawData
            })
    except :
        pass

result["import"] = {}
try :
    for entry in pe.DIRECTORY_ENTRY_IMPORT:
        try :
            result["import"][entry.dll.decode('utf-8')] = []
            for imp in entry.imports:
                result["import"][entry.dll.decode('utf-8')].append( imp.name.decode('utf-8'))
        except :
            pass
except :
    pass

print( result )
f = open(os.path.join("result",result["hash"]+".json"),"w")
json.dump( result, f, indent=4 )
f.close()

